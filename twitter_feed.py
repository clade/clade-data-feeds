import tweepy as tw
from tweepy import TweepError
from lib.schedule_helper import ScheduleHelper
from lib.mongo import Mongo
from datetime import datetime
from time import sleep
from lib.twitter_company_sentiment import TwitterCompanySentiment
from lib.twitter_user import TwitterUser
from lib.company_helper import CompanyHelper
from twitter_analysis.clean_text import CleanText
from twitter_analysis.generate_text import GenerateText


class TwitterFeed:

    tweets = None
    tweets_text = []

    def __init__(self, tweet_size=25):
        self.db = Mongo().db

        self.get_twitter_ticker_schedule_config = self.db.schedule.find_one(
            {"job": "get_twitter_ticker"}
        )
        self.tweet_size = tweet_size
        self.__twitter_api_connection()

    def __twitter_api_connection(self):
        config = self.db.twitter_config.find_one()

        consumer_key = config["consumer_key"]
        consumer_secret = config["consumer_secret"]
        access_token = config["access_token"]
        access_token_secret = config["access_token_secret"]

        auth = tw.OAuthHandler(consumer_key, consumer_secret)
        auth.set_access_token(access_token, access_token_secret)
        self.api = tw.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)

    def get_last_known_tweet_id(self, symbol):
        record = self.db.twitter_feed.find({"symbol": symbol}).sort("id", -1).limit(1)
        tweet_id = None
        for r in record:
            tweet_id = r["id"]
            break
        return tweet_id

    def search_tweets(self, name, symbol):
        search_words = f"{name.split(' ')[0]} ${symbol}"
        tweets_data = []

        args = {
            "method": self.api.search,
            "q": search_words,
            "lang": "en",
            "result_type": "mixed",
            "count": self.tweet_size,
            "tweet_mode": "extended",
        }
        tweet_id = self.get_last_known_tweet_id(symbol)
        if tweet_id is not None:
            args["since_id"] = tweet_id

        try:
            for tweet in tw.Cursor(**args).items(self.tweet_size):
                
                if hasattr(tweet, 'retweeted_status'):
                    text = tweet.retweeted_status.full_text
                else:
                    text = tweet.full_text

                tweets_data.append(
                    {
                        "created_at": tweet.created_at,
                        "text": text,
                        "followers_count": tweet.user.followers_count,
                        "screen_name": tweet.user.screen_name,
                        "entities": tweet.entities,
                        "id_str": tweet.id_str,
                        "user": tweet.user.name,
                        "user_id": tweet.user.id,
                        "id": tweet.id,
                        "favorite_count": tweet.favorite_count,
                        "retweet_count": tweet.retweet_count,
                        "comments": len(tweet.entities["hashtags"]),
                    }
                )
        except TweepError:
            print("Tweepy Error moving to next target")
            return []

        return tweets_data

    def save_tweets(self, symbol, tweets):
        for tweet in tweets:
            sentiment = TwitterCompanySentiment().get_tweet_sentiment(tweet)
            link_sentiment = TwitterCompanySentiment().get_links_sentiment(tweet)
            tweet["sentiment"] = {
                "polarity": sentiment.polarity,
                "subjectivity": sentiment.subjectivity,
            }
            tweet["link_sentiment"] = link_sentiment
            self.db.twitter_feed.find_one_and_replace(
                {"symbol": symbol, "id": tweet["id"]},
                {"symbol": symbol, "id": tweet["id"], "values": tweet},
                upsert=True,
            )
            TwitterUser().get_user_stats(tweet["user_id"])

    def convert_to_date(self, date):
        return datetime.strptime(date, "%Y-%M-%d")

    def start(self):
        while not sleep(1):
            df_schedule = ScheduleHelper(self.get_twitter_ticker_schedule_config)
            if df_schedule.within_time():
                df_period = df_schedule.period() + (0)

                for company in CompanyHelper.next_company_position("twitter_feed"):
                    tweets_data = self.search_tweets(company["name"], company["symbol"])
                    self.save_tweets(company["symbol"], tweets_data)
                    print(
                        f"Recorded {len(tweets_data)} with {company['symbol']}/{company['name']} Next Twitter Feed call in {df_period / 60} minute"
                    )
                    GenerateText().start()
                    CleanText().start()
                    sleep(df_period)


TwitterFeed().start()
