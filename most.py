from yahoo_fin.stock_info import get_day_gainers
from yahoo_fin.stock_info import get_day_most_active
from lib.mongo import Mongo
from bson.json_util import dumps
from time import sleep, time
from lib.schedule_helper import ScheduleHelper
import json


class Most:
    db = None
    company_data = None
    start_time = None

    def __init__(self):
        self.db = Mongo().db
        self.company_data = list(json.loads(dumps(self.db.company_data.find())))
        self.get_most_schedule_config = self.db.schedule.find_one({"job": "get_most"})
        self.start_time = time()

    def save_most_data_to_db(self, data, collection):
        index = 0
        symbols = json.loads(data)["Symbol"]
        change = json.loads(data)["% Change"]

        companies = [company.get("symbol") for company in self.company_data]
        to_save = []

        for sym in symbols:
            symbol_value = symbols[sym]
            change_value = change[sym]
            if symbols[sym] in companies:
                to_save.append(
                    {
                        "symbol": symbol_value,
                        "percent_change": change_value,
                        "index": index,
                    }
                )
                index = index + 1
                if index == 10:
                    break
        collection.remove()
        if len(to_save) is not 0:
            collection.insert_many(to_save)

    def get_most_gains(self):
        return get_day_gainers().to_json()

    def get_most_active(self):
        return get_day_most_active().to_json()

    def start(self):
        while not sleep(1):
            schedule = ScheduleHelper(self.get_most_schedule_config)
            if schedule.within_time():
                period = schedule.period()
                most_active = self.get_most_active()
                self.save_most_data_to_db(most_active, self.db.most_active)
                most_gains = self.get_most_gains()
                self.save_most_data_to_db(most_gains, self.db.most_gains)
                print(f"Next Most call in {period / 60} minutes")
                sleep(period)


Most().start()
