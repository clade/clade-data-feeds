#!/usr/bin/env python3
from datetime import datetime
from newsapi.newsapi_client import NewsApiClient
from newsapi.newsapi_exception import NewsAPIException
from time import sleep
from config.news_config import api_key
from lib.mongo import Mongo
from lib.schedule_helper import ScheduleHelper
import time


class News:
    def __init__(self):
        self.db = Mongo().db
        self.news_headlines_schedule_config = self.db.schedule.find_one(
            {"job": "get_news_headlines"}
        )
        self.news_api = NewsApiClient(api_key=api_key)
        self.start_time = time.time()

    def save_headlines_to_db(self, news_data):
        self.db.news_headlines.drop()
        for news_article in news_data:
            self.db.news_headlines.insert_one(
                {
                    "title": news_article["title"],
                    "article": {
                        "source": news_article["source"],
                        "description": news_article["description"],
                        "url": news_article["url"],
                        "url_image": news_article["urlToImage"],
                        "content": news_article["content"],
                        "author": news_article["author"],
                    },
                    "published_at": datetime.strptime(
                        news_article["publishedAt"], "%Y-%m-%dT%H:%M:%SZ"
                    ),
                }
            )

    def __one_hour_wait(self):
        print("Too many requests wait for 1 hour...")
        return sleep(60 * 60)

    def get_top_headlines(self):
        try:
            return self.news_api.get_top_headlines(language="en", country="us")[
                "articles"
            ]
        except NewsAPIException:
            self.__one_hour_wait()
            return []

    def start(self):

        while not sleep(1):
            news_schedule = ScheduleHelper(self.news_headlines_schedule_config)
            if news_schedule.within_time():
                news_period = news_schedule.period()
                top_headlines = self.get_top_headlines()
                self.save_headlines_to_db(top_headlines)
                print(f"Next news call in {news_period / 60} minutes")
                sleep(news_period)


News().start()
