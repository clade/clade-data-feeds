from lib.ticker_data_helper import TickerData
from lib.interval_enum import Interval

TickerData(Interval.WEEK).start()
