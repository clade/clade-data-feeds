from lib.mongo import Mongo
from lib.schedule_helper import ScheduleHelper
from time import sleep
from yahoo_fin.stock_info import get_quote_table
from bson.json_util import dumps
from time import time
import json


class Fundamentals:
    db = None
    company_data = None
    start_time = None
    earliest_dividend_date = "01-01-2010"

    def __init__(self):
        self.db = Mongo().db
        self.company_data = list(json.loads(dumps(self.db.company_data.find())))
        self.start_time = time()

    def fix_nans(self, field):
        if field != field:
            return 0.0
        else:
            return field

    def save_df_data_to_db(self, symbol, company_data):
        print(f"Company Data for: {symbol}")

        try:
            market_cap = company_data["Market Cap"]
            fifty_week_range = company_data["52 Week Range"].split("-")
            fifty_low = float(fifty_week_range[0].replace(",", ""))
            fifty_high = float(fifty_week_range[1].replace(",", ""))
            earnings_date = company_data["Earnings Date"]
            pe_ratio = self.fix_nans(company_data["PE Ratio (TTM)"])
            previous_close = self.fix_nans(company_data["Previous Close"])
            volume = company_data["Volume"]

            company_data = {
                "symbol": symbol,
                "market_cap": market_cap,
                "fifty_low": fifty_low,
                "fifty_high": fifty_high,
                "earnings_date_range": earnings_date,
                "pe_ratio": pe_ratio,
                "previous_close": previous_close,
                "volume": volume,
            }
            self.db.company_fundamentals.find_one_and_replace(
                {"symbol": symbol}, company_data, upsert=True
            )
        except:
            print(f"Failed to get company fundamentals for {symbol}")

    def get_company_data_for(self, q):
        try:
            company = get_quote_table(q)
        except:
            print("catch all errors!")
            company = {}

        return company

    def get_yahoo_ready_symbol(self, symbol):
        return symbol.replace(".", "-")

    def get_schedule_config(self):
        return self.db.schedule.find_one({"job": "get_company_fundamentals"})

    def start(self):

        while not sleep(1):
            df_schedule = ScheduleHelper(self.get_schedule_config())
            if df_schedule.within_time():
                df_period = df_schedule.period()
                for company in self.company_data:
                    symbol = self.get_yahoo_ready_symbol(company["symbol"])
                    company_data = self.get_company_data_for(symbol)
                    self.save_df_data_to_db(company["symbol"], company_data)
                    print(f"Company Fundamentals call in {df_period / 60} minutes")
                    sleep(df_period)
            else:
                print("Outside hours sleep for an extra hour")
                sleep(60 * 60)


Fundamentals().start()
