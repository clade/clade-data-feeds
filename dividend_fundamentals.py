from lib.mongo import Mongo
from bson.json_util import dumps
from lib.schedule_helper import ScheduleHelper
from time import sleep
from yahoo_fin.stock_info import get_dividends
from yahoo_fin.stock_info import get_quote_table
from datetime import datetime
import time
import json
import re


class DividendFundamentals:
    db = None
    company_data = None
    start_time = None
    earliest_dividend_date = "01-01-2010"

    def __init__(self):
        self.db = Mongo().db
        self.company_data = list(json.loads(dumps(self.db.company_data.find())))
        self.dividend_fundamentals_schedule_config = self.db.schedule.find_one(
            {"job": "get_dividend_fundamentals"}
        )
        self.start_time = time.time()

    def convert_milli_timestamp_to_date(self, milli_string):
        return datetime.fromtimestamp(int(milli_string) / 1000)

    def save_df_data_to_db(self, symbol, dividend_data):
        print(f"Dividends Data for: {symbol}")

        if len(dividend_data["dividend_fundamentals"]) == 0:
            print("No dividend fundamentals, moving to next record")
            return
        else:
            dividend_fundamentals = dividend_data["dividend_fundamentals"]

        if len(dividend_data["dividend_history"]) != 0:
            print("No dividend_history, moving to next record")
            dividends_history = dividend_data["dividend_history"]["dividend"]
            dividend_dates = self.get_dividend_historical_data(dividends_history)
        else:
            dividend_dates = []

        result = re.findall(
            r"([\d+.]*\d+)", dividend_fundamentals["Forward Dividend & Yield"]
        )
        try:
            forward_annual_dividend_rate = float(result[0])
            dividend_yield = float(result[1]) / 100
            dividend_ex_date = datetime.strptime(
                dividend_fundamentals["Ex-Dividend Date"], "%b %d, %Y"
            )
        except:
            forward_annual_dividend_rate = 0
            dividend_yield = 0
            dividend_ex_date = None

        dividends_data = {
            "symbol": symbol,
            "dividend_ex_date": dividend_ex_date,
            "forward_annual_dividend_rate": forward_annual_dividend_rate,
            "dividend_yield": dividend_yield,
            "dividend_history": dividend_dates,
        }
        self.db.dividends.find_one_and_replace(
            {"symbol": symbol}, dividends_data, upsert=True
        )

    def get_dividend_historical_data(self, dividends_history):
        dividend_dates = []
        for key, value in dividends_history.items():
            dividend_dates.append(
                {"date": self.convert_milli_timestamp_to_date(key), "value": value}
            )
        return dividend_dates

    def get_dividend_data_for(self, q):
        try:
            dividends = get_dividends(q, self.earliest_dividend_date)
            dividends_history = json.loads(dividends.to_json())
        except:
            print("catch all errors!")
            dividends_history = {}

        try:
            dividend_fundamentals = get_quote_table(q)
        except:
            print("catch all errors!")
            dividend_fundamentals = {}

        return {
            "dividend_history": dividends_history,
            "dividend_fundamentals": dividend_fundamentals,
        }

    def get_yahoo_ready_symbol(self, symbol):
        return symbol.replace(".", "-")

    def start(self):

        while not sleep(1):
            df_schedule = ScheduleHelper(self.dividend_fundamentals_schedule_config)
            if df_schedule.within_time():
                df_period = df_schedule.period()
                for company in self.company_data:
                    symbol = self.get_yahoo_ready_symbol(company["symbol"])
                    dividends_data = self.get_dividend_data_for(symbol)
                    self.save_df_data_to_db(company["symbol"], dividends_data)
                    print(f"Next dividends call in {df_period / 60} minutes")
                    sleep(df_period)
            else:
                print("Outside hours sleep for an extra hour")
                sleep(60 * 60)


DividendFundamentals().start()
