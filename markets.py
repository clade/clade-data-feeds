from lib.mongo import Mongo
from time import sleep, time
from bson.json_util import dumps
from lib.schedule_helper import ScheduleHelper
import json
import requests


class Markets:

    url = "https://query1.finance.yahoo.com/v7/finance/spark?symbols="

    def __init__(self):
        self.db = Mongo().db
        self.market_symbols = list(json.loads(dumps(self.db.market_data.find())))
        self.get_markets_schedule_config = self.db.schedule.find_one(
            {"job": "get_markets"}
        )
        self.start_time = time()

    def delay_request_to_yahoo(self):
        sleep(1)

    def save_markets_data_to_db(self, market_data):

        for market in market_data:
            result = market["spark"]["result"][0]
            symbol = result["symbol"]
            meta = result["response"][0]["meta"]
            previous_close = meta["previousClose"]
            regular_market_price = meta["regularMarketPrice"]
            market_json = {
                "symbol": symbol,
                "previous_close": previous_close,
                "regular_market_price": regular_market_price,
            }

            self.db.markets.find_one_and_replace(
                {"symbol": symbol}, market_json, upsert=True
            )

    def get_market_data(self):
        data = []
        for ms in self.market_symbols:
            symbol = ms["symbol"]
            request_url = f"{self.url}{symbol}"
            symbol_data = requests.get(request_url)
            data.append(json.loads(symbol_data.text))
            self.delay_request_to_yahoo()

        return data

    def start(self):
        while not sleep(1):
            schedule = ScheduleHelper(self.get_markets_schedule_config)
            if schedule.within_time():
                period = schedule.period()
                market_data = self.get_market_data()
                self.save_markets_data_to_db(market_data)
                print(f"Next Markets call in {period / 60} minutes")
                sleep(period)


Markets().start()
