#!/usr/bin/env python3
import time
from pymongo import MongoClient
from bson.json_util import dumps
import json
from twelvedata import TDClient
import requests
from datetime import datetime, date
from twelvedata.exceptions import TwelveDataError

db = MongoClient(
    "mongodb+srv://clade-db-001:Sorento!023@cluster0.poh3r.mongodb.net/clade?retryWrites=true&w=majority"
)
st = time.time()
tw_stocks_url = "https://api.twelvedata.com/stocks"
tw_client = TDClient(apikey="7ebcb86a798040b3ad81c5f86f2def84")


def chunks(chunk_list, chunk_size):
    chunk_size = max(1, chunk_size)
    return list(
        (chunk_list[i : i + chunk_size] for i in range(0, len(chunk_list), chunk_size))
    )


def get_stocks_data_from_exchange(it, exchange, stock_type):
    if it["exchange"] == exchange and it["type"] == stock_type:
        return True
    return False


def get_common_stocks_by_exchange(exchange):
    response = requests.get(tw_stocks_url)
    json_resp = json.loads(response.text)
    common_stock_filter = filter(
        lambda stock: get_stocks_data_from_exchange(stock, exchange, "Common Stock"),
        json_resp["data"],
    )
    return list(common_stock_filter)


def limited_stock_london():
    with open("limited_stock_london.json") as f:
        data = json.load(f)
    return data


def save_ticker_data_to_mongo(stock_data):
    for symbol in stock_data:
        if db.clade.stock_symbol.count_documents({"_id": symbol}) == 0:
            db.clade.stock_symbol.insert_one({"_id": symbol})

        save_tick_to_mongo(symbol, stock_data[symbol])


def save_tick_to_mongo(symbol, symbol_data):
    for data in symbol_data:
        if (
            db.clade.time_series.count_documents(
                {"parent": symbol, data["datetime"]: {"$exists": True}}
            )
            == 0
        ):
            print(
                "*** Updated {} - {} - O: {} ***".format(
                    symbol, data["datetime"], data["open"]
                )
            )
            db.clade.time_series.insert_one(
                {
                    "parent": symbol,
                    data["datetime"]: {
                        "open": data["open"],
                        "high": data["high"],
                        "low": data["low"],
                        "close": data["close"],
                        "volume": data["volume"],
                    },
                }
            )


def start_timer():
    exchange = "LSE"
    for timer in list(get_config_timers()):
        if timer["name"] == exchange:
            lse_exchange_details = get_exchange_data(exchange)

            while True:
                lse_utc_close, lse_utc_open, week_day = lse_opening_times(
                    lse_exchange_details
                )
                if week_day:
                    if lse_utc_open < utc_now() < lse_utc_close:
                        common_stocks = limited_stock_london()
                        stock_chunks = chunks(common_stocks, 12)

                        for stock_chunk in stock_chunks:
                            chunk_time_delay = 120
                            stock_data = get_lse_stock_chunk(stock_chunk, timer)
                            save_ticker_data_to_mongo(stock_data)
                            print(
                                "*** CHUNK WAIT FOR: {} seconds ***".format(
                                    chunk_time_delay
                                )
                            )
                            time.sleep(
                                chunk_time_delay
                                - ((time.time() - st) % chunk_time_delay)
                            )

                    period_time_delay = 960
                    print(
                        "*** PERIOD WAIT FOR: {} minutes from {} ***".format(
                            (period_time_delay / 60), datetime.now()
                        )
                    )
                    time.sleep(
                        period_time_delay - ((time.time() - st) % period_time_delay)
                    )


def lse_opening_times(lse_exchange_details):
    lse_utc_open = convert_time(lse_exchange_details["utc_open"])
    lse_utc_close = convert_time(lse_exchange_details["utc_close"])
    return lse_utc_close, lse_utc_open, weekday()


def get_lse_stock_chunk(stock_chunk, timer):
    symbols_list = list(map(lambda x: x["symbol"], stock_chunk))
    stocks = ",".join(symbols_list)
    try:
        twelve = tw_client.time_series(
            symbol=stocks,
            interval=timer["interval"],
            outputsize=100,
            timezone="Europe/London",
        )
        return twelve.as_json()
    except TwelveDataError as e:
        print("ERROR Twelve Data: {}".format(e))
        return []


def get_config_timers():
    return json.loads(dumps(db.clade.config_ticker.find()))


def get_exchange_data(exchange):
    exchanges = list(json.loads(dumps(db.clade.exchanges.find())))
    first_exchange = first(
        exchanges, default=1, condition=lambda x: x["name"] == exchange
    )
    return first_exchange


def first(iterable, default=None, condition=lambda x: True):
    try:
        return next(x for x in iterable if condition(x))
    except StopIteration:
        if default is not None and condition(default):
            return default
        else:
            raise


def utc_now():
    return datetime.utcnow()


def convert_time(utc_time):
    today_with_utc_time = date.today().strftime("%d/%m/%Y") + " " + utc_time
    to_date_time = datetime.strptime(today_with_utc_time, "%d/%m/%Y %H:%M")
    return to_date_time


def weekday():
    if datetime.today().weekday() < 5:
        return True
    return False


start_timer()
