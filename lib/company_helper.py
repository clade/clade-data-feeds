import json
from collections import deque
from lib.mongo import Mongo
from bson.json_util import dumps


class CompanyHelper:
    @staticmethod
    def next_company_position(collection):
        mongo = Mongo(collection)
        company_data = list(json.loads(dumps(mongo.db.company_data.find())))
        last_executed = mongo.collection.find({}, {"symbol"}).sort("id", -1).limit(1)

        symbol = None
        for c in last_executed:
            symbol = c["symbol"]
            break

        if symbol:
            symbol_index = next(
                (
                    index
                    for (index, d) in enumerate(company_data)
                    if d["symbol"] == symbol
                ),
                None,
            )
            company_list_deque = deque(company_data)
            company_list_deque.rotate(len(company_data) - (symbol_index + 1))
            return company_list_deque
        else:
            return company_data
