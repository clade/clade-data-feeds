import tweepy as tw
from tweepy.error import TweepError
from lib.mongo import Mongo


class TwitterUser:
    def __init__(self):
        self.db = Mongo().db
        self.__twitter_api_connection()

    def __twitter_api_connection(self):
        config = self.db.twitter_config.find_one()

        consumer_key = config["consumer_key"]
        consumer_secret = config["consumer_secret"]
        access_token = config["access_token"]
        access_token_secret = config["access_token_secret"]

        auth = tw.OAuthHandler(consumer_key, consumer_secret)
        auth.set_access_token(access_token, access_token_secret)
        self.api = tw.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)

    def get_user_stats(self, user_id):
        try:

            user = self.api.get_user(user_id)
            meta = {
                "favourites_count": user.favourites_count,
                "followers_count": user.followers_count,
                "friends_count": user.friends_count,
                "verified": user.verified,
                "listed_count": user.listed_count,
                "created_at": user.created_at,
                "name": user.name,
                "screen_name": user.screen_name,
            }
            self.db.twitter_user.find_one_and_replace(
                {"user_id": user_id},
                {"user_id": user_id, "meta": meta},
                upsert=True,
            )

        except TweepError:
            pass