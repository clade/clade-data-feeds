import json
import time
from twelvedata import TDClient
from lib.mongo import Mongo
from bson.json_util import dumps
from time import sleep, time
from lib.interval_enum import Interval
from lib.schedule_helper import ScheduleHelper
from config.televe_data_config import api_key
from datetime import datetime


class TickerData:
    def __init__(self, interval):
        self.interval = interval
        self.db = Mongo().db
        self.company_data = list(json.loads(dumps(self.db.company_data.find())))
        self.get_ticker_schedule_config = self.db.schedule.find_one(
            {"job": "get_ticker"}
        )
        self.tw_client = TDClient(apikey=api_key)
        self.start_time = time()

    def get_ticker_data(self, symbol):
        ts = self.tw_client.time_series(
            symbol=symbol,
            interval=self.interval.value,
            outputsize=50,
            timezone="America/New_York",
        )
        json_data = ts.as_json()
        return json_data

    def interval_collection(self):
        if self.interval == Interval.MIN_30:
            return self.db.time_series_min_30
        if self.interval == Interval.DAY:
            return self.db.time_series_day
        if self.interval == Interval.WEEK:
            return self.db.time_series_week
        if self.interval == Interval.MONTH:
            return self.db.time_series_month
        else:
            raise Exception(f"interval not in play! {self.interval}")

    def convert_to_date(self, date):
        if self.interval == Interval.DAY:
            return datetime.strptime(date, "%Y-%M-%d")

    def save_tick_to_mongo(self, symbol, symbol_data):
        for data in symbol_data:
            self.interval_collection().find_one_and_replace(
                {"symbol": symbol, "values.datetime": data["datetime"]},
                {
                    "symbol": symbol,
                    "values": {
                        "datetime": self.convert_to_date(data["datetime"]),
                        "open": data["open"],
                        "high": data["high"],
                        "low": data["low"],
                        "close": data["close"],
                        "volume": data["volume"],
                    },
                },
                upsert=True,
            )

    def start(self):

        while not sleep(1):
            news_schedule = ScheduleHelper(self.get_ticker_schedule_config)
            if news_schedule.within_time():
                news_period = news_schedule.period()
                for company in self.company_data:
                    symbol = company["symbol"]
                    ticker_data = self.get_ticker_data(symbol)
                    self.save_tick_to_mongo(symbol, ticker_data)
                    print(
                        f"Completed {self.interval.value} for {symbol} - Next call in {news_period / 60} minutes"
                    )
                    sleep(news_period)
