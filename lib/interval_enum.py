from enum import Enum


class Interval(Enum):
    MIN_30 = "30min"
    DAY = "1day"
    WEEK = "1week"
    MONTH = "1month"
