from pymongo import MongoClient


class Mongo:
    db = None
    colection = None

    def __init__(self, collection=None):
        client = MongoClient("mongodb://localhost:27017/")
        self.db = client["cassius"]
        if collection:
            self.collection = self.db.get_collection(collection)
