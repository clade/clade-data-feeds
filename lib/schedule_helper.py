from datetime import datetime


class ScheduleHelper:

    schedule = None

    def __init__(self, schedule):
        self.schedule = schedule

    def __convert_time(self, tm):
        try:
            if tm is None:
                return None
            else:
                return datetime.strptime(tm, "%H:%M").time()
        except ValueError:
            return False

    def __start_time(self):
        return self.__convert_time(self.schedule["start_time"])

    def __end_time(self):
        return self.__convert_time(self.schedule["end_time"])

    def within_time(self):
        s = self.__start_time() is None or self.__start_time() < datetime.now().time()
        e = self.__end_time() is None or self.__end_time() > datetime.now().time()
        if s and e:
            return True
        else:
            return False

    def period(self):
        return self.schedule["period"] * 60
