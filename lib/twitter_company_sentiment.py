import sys
import json
import time
import re
import nltk
import requests, requests.exceptions
from requests.exceptions import (
    SSLError,
    InvalidURL,
    ReadTimeout,
    ConnectTimeout,
    ConnectionError,
    ContentDecodingError,
)
import argparse
import logging
import string
import pke
from pke import compute_document_frequency
from string import punctuation
from nltk.corpus import stopwords

try:
    import urllib.parse as urlparse
except ImportError:
    import urlparse
import tweepy
from textblob import TextBlob, tokenizers
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
from bs4 import BeautifulSoup
from random import randint, randrange
from datetime import datetime
from langdetect import detect, LangDetectException
from deep_translator.exceptions import RequestError
import spacy
from pattern.en import sentiment
from deep_translator import GoogleTranslator

IS_PY3 = sys.version_info >= (3, 0)
nlp = spacy.load("en_core_web_sm")

if not IS_PY3:
    print("Requires Python 3.x")
    sys.exit(1)


class TwitterCompanySentiment:

    MINIMUM_TEXT_BODY = 75

    def __init__(self):
        self.count = 0
        self.extractor = pke.unsupervised.TopicRank()

    def vader_score(self, text):
        analyzer = SentimentIntensityAnalyzer()
        vs = analyzer.polarity_scores(text)
        return str(vs)

    def text_blob(self, text):
        blob = TextBlob(text)
        return sum(c.polarity for c in blob.sentences)

    def analyse(self, text):
        vader = self.vader_score(text)
        text_blob = self.text_blob(text)
        return vader, text_blob

    def links(self, text):
        return re.findall(r'(https?://\S+)', text)

    def clean_text(self, text):

        text = re.sub(r'\'s', ' ', text)
        to_rm = ":,-?><!£$%^&*()_-+=()|/'" #Leave . for sentence analysis
        for c in list(to_rm):
            text = text.replace(c, '')

        text = text.replace("\n", " ")
        text = re.sub(r"https?\S+", "", text)
        text = re.sub(r"&.*?;", "", text)
        text = re.sub(r"<.*?>", "", text)
        text = text.replace("RT", "")
        text = re.sub(r"[#|@]\S+", "", text)
        text = re.sub(r'\t', ' ', text)
        text = re.sub(r'\r', ' ', text)
        text = re.sub(r'\n', ' ', text)
        
        text = re.sub("\s\s+", " ", text)
        text = re.sub(r'[0-9]', '', text)

        return text

    def keywords(self, text, extractor=10):
        self.extractor = pke.unsupervised.YAKE()
        stoplist = stopwords.words("english")
        self.extractor.load_document(text, language="en", normalization=None)
        self.extractor.candidate_selection(n=extractor, stoplist=stoplist)
        self.extractor.candidate_weighting()
        pos = {"NOUN", "PROPN", "ADJ"}
        use_stems = False  # use stems instead of words for weighting
        keyphrases1 = self.extractor.get_n_best(n=extractor)

        # pos = {'NOUN', 'PROPN', 'ADJ'}
        # self.extractor = pke.unsupervised.TextRank()
        # self.extractor.load_document(text, language='en', normalization=None)
        # self.extractor.candidate_weighting(window=2, pos=pos, top_percent=0.66)
        # keyphrases2 = self.extractor.get_n_best(n=extractor)
        return keyphrases1

    def get_links_sentiment(self, tweet):
        links_sentiment = []
        tweet_links = self.links(tweet["text"])

        for link in tweet_links:
            try:
                response = requests.get(link, timeout=30)
            except (
                SSLError,
                InvalidURL,
                ReadTimeout,
                ConnectTimeout,
                ConnectionError,
                ContentDecodingError,
            ) as e:
                print(f"Request Error with {link} - Type type {type(e)}")
                return []

            def __proceed(resp):
                trusted_host = True
                exclusion_list = [
                    "twitter.com",
                    "amazon.com",
                    "aiera.com",
                    "fwbusiness.com",
                ]
                for exclusion in exclusion_list:
                    if re.search(exclusion, resp.url, re.IGNORECASE):
                        trusted_host = False
                        break

                if trusted_host and not resp.status_code == 403:
                    return True

                return False

            if __proceed(response):
                html = response.text
                soup = BeautifulSoup(html, "html.parser")
                text = soup.get_text()

                # Check for non english text
                try:
                    detected = detect(text)
                    if detected != "en":
                        text = GoogleTranslator(source="auto", target="en").translate(
                            text[:4995]
                        )
                except (LangDetectException, RequestError) as e:
                    print(f"Ignore lang detection!: {e}")

                clean_text = self.clean_text(text)
                
                # Max article size
                clean_text = clean_text[:900000]

                if len(clean_text) > self.MINIMUM_TEXT_BODY:

                    h1s = soup.find_all(["h1"]); 
                    h1s_text = [h1.get_text() for h1 in h1s]

                    h2s = soup.find_all(["h2"]); 
                    h2s_text = [h2.get_text() for h2 in h2s]

                    sentence = []
                    tokens = nlp(clean_text)
                    for sent in tokens.sents:
                        sentence.append(sent.text.strip())

                    textblob_sentiment = []
                    for s in sentence:
                        txt = TextBlob(s)
                        polarity = txt.sentiment.polarity
                        subjectivity = txt.sentiment.subjectivity
                        textblob_sentiment.append([s, polarity, subjectivity])

                    pattern_sentiment = []
                    for s in sentence:
                        res = sentiment(s)
                        polarity = res[0]
                        subjectivity = res[1]
                        pattern_sentiment.append([s, polarity, subjectivity])

                    tokenizer = nltk.tokenize.RegexpTokenizer(
                        pattern="\w+|\$[\d\.]+|\S+"
                    )
                    tokens = tokenizer.tokenize(clean_text)
                    words, words_new = [], []
                    for word in tokens:
                        words.append(word.lower())

                    stopwords = nltk.corpus.stopwords.words("english")
                    stopwords.extend(["all", "due", "to", "on", "daily", "twitter", "get", "company"])
                    for word in words:
                        if word not in stopwords and word not in string.punctuation:
                            words_new.append(word)

                    freq_dist = nltk.FreqDist(words_new)

                    links_sentiment.append(
                        {
                            "link": response.url,
                            "textblob_sentiment": textblob_sentiment,
                            "pattern_sentiment": pattern_sentiment,
                            "freq_distribution": freq_dist.most_common(10),
                            "h1": h1s_text,
                            "h2": h2s_text
                        }
                    )

        return links_sentiment

    def get_tweet_sentiment(self, tweet):
        text_clean = self.clean_text(tweet["text"])

        return TextBlob(text_clean).sentiment
