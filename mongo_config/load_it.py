import json
from pymongo import MongoClient
import argparse

client = MongoClient("mongodb://localhost:27017/")
mongo = client["cassius"]


def drop_collection(collection):
    mongo[collection].drop()


def create_collection(collection_json, collection_name):
    collection = mongo[collection_name]
    with open(collection_json) as json_file:
        data = json.load(json_file)
        print("Collection {0} created: {1} entries".format(collection_name, len(data)))
        collection.insert_many(data)


parser = argparse.ArgumentParser(description="Create Reference Data")
parser.add_argument("--data", help="Enter the reference data to insert")

configs = [
    "company_data.json",
    "schedule.json",
    "market_data.json",
    "twitter_config.json",
]


def collections(collection_file):
    print(collection_file)
    collection_name = collection_file.replace(".json", "")
    drop_collection(collection_name)
    create_collection(collection_file, collection_name)


args = parser.parse_args()
if args.data == "all":
    for collection_file in configs:
        collections(collection_file)
elif args.data in configs:
    collections(args.data)
else:
    print("Data point does not exist: {0}".format(args.data))
