from lib.mongo import Mongo
from bson.json_util import dumps
from lib.schedule_helper import ScheduleHelper
from time import sleep, time
from alpha_vantage.fundamentaldata import FundamentalData
from datetime import datetime
from config.alpha_vantage_config import api_key
import json


class DividendFundamentals2:
    db = None
    company_data = None
    start_time = None
    earliest_dividend_date = "01-01-2010"
    fundamental_data = None

    def __init__(self):
        self.db = Mongo().db
        self.company_data = list(json.loads(dumps(self.db.company_data.find())))
        self.dividend_fundamentals_schedule_config = self.db.schedule.find_one(
            {"job": "get_dividend_fundamentals"}
        )
        self.start_time = time()
        self.fundamental_data = FundamentalData(key=api_key)

    def convert_milli_timestamp_to_date(self, milli_string):
        return datetime.fromtimestamp(int(milli_string) / 1000)

    def convert_date(self, dt):
        try:
            return datetime.strptime(dt, "%Y-%m-%d")
        except ValueError as e:
            return None

    def save_df_data_to_db(self, symbol, dividend_data):
        print(f"Dividends Data for: {symbol}")
        if len(dividend_data) > 0:
            ex_dividend_rate = self.convert_date(dividend_data[0]["ExDividendDate"])
            forward_annual_dividend_rate = dividend_data[0]["ForwardAnnualDividendRate"]
            dividend_yield = dividend_data[0]["DividendYield"]
            dividend_date = self.convert_date(dividend_data[0]["DividendDate"])

            dividends_data = {
                "symbol": symbol,
                "dividend_ex_date": ex_dividend_rate,
                "forward_annual_dividend_rate": forward_annual_dividend_rate,
                "dividend_yield": dividend_yield,
                "dividend_date": dividend_date,
            }
            self.db.dividends.update_one(
                {"symbol": symbol}, {"$set": dividends_data}, upsert=True
            )

    def get_dividend_data_for(self, q):
        try:
            return self.fundamental_data.get_company_overview(symbol=q)
        except ValueError as e:
            print(f"Error raised for {q}: {e}")
            return []

    def start(self):

        while not sleep(1):
            df_schedule = ScheduleHelper(self.dividend_fundamentals_schedule_config)
            if df_schedule.within_time():
                df_period = df_schedule.period()
                for company in self.company_data:
                    dividends_data = self.get_dividend_data_for(company["symbol"])
                    self.save_df_data_to_db(company["symbol"], dividends_data)
                    print(f"Next dividends call in {df_period / 60} minutes")
                    sleep(df_period)
            else:
                print("Outside hours sleep for an extra hour")
                sleep(60 * 60)


DividendFundamentals2().start()
