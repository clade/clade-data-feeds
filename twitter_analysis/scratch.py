import tweepy
from pymongo import MongoClient

client = MongoClient("mongodb://localhost:27017/")
db = client["cassius"]

config = db.twitter_config.find_one()

consumer_key = config["consumer_key"]
consumer_secret = config["consumer_secret"]
access_token = config["access_token"]
access_token_secret = config["access_token_secret"]

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)

# assign the values accordingly
consumer_key = config["consumer_key"]
consumer_secret = config["consumer_secret"]
access_token = config["access_token"]
access_token_secret = config["access_token_secret"]

# authorization of consumer key and consumer secret
auth = tweepy.OAuthHandler(consumer_key, consumer_secret)

# set access to user's access key and access secret
auth.set_access_token(access_token, access_token_secret)

# calling the api
api = tweepy.API(auth)

# the ID of the status
id = 1474372832096407552
# id = 1474412185292050447

# fetching the status
tweet = api.get_status(id, tweet_mode="extended")
if hasattr(tweet, "retweeted_status"):
    text = tweet.retweeted_status.full_text
else:
    text = tweet.full_text

# fetching the text attribute
# text = status.text

print("The text of the status is : \n\n" + text)
