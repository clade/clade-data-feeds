from sklearn.model_selection import train_test_split
from pandas import pandas as pd
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer

# vectorizer = CountVectorizer()
vectorizer = TfidfVectorizer()

df = pd.read_csv(
    "/Users/sam/Desktop/Clade/project-code/clade-data-feeds/twitter_analysis/twitter_data_cleaned.csv"
)
df = df.dropna()
vectorizer.fit(df["text"])
vector = vectorizer.transform(df["text"])

stopwordlist = [
    "a",
    "about",
    "above",
    "after",
    "again",
    "ain",
    "all",
    "am",
    "an",
    "and",
    "any",
    "are",
    "as",
    "at",
    "be",
    "because",
    "been",
    "before",
    "being",
    "below",
    "between",
    "both",
    "by",
    "can",
    "d",
    "did",
    "do",
    "don",
    "does",
    "doing",
    "down",
    "during",
    "each",
    "few",
    "for",
    "from",
    "further",
    "had",
    "has",
    "have",
    "having",
    "he",
    "her",
    "here",
    "hers",
    "herself",
    "him",
    "himself",
    "his",
    "how",
    "i",
    "if",
    "in",
    "into",
    "is",
    "it",
    "its",
    "itself",
    "just",
    "ll",
    "m",
    "ma",
    "me",
    "more",
    "most",
    "my",
    "myself",
    "now",
    "o",
    "of",
    "on",
    "once",
    "only",
    "or",
    "other",
    "our",
    "ours",
    "ourselves",
    "out",
    "own",
    "re",
    "s",
    "same",
    "she",
    "shes",
    "should",
    "shouldve",
    "so",
    "some",
    "such",
    "t",
    "than",
    "that",
    "thatll",
    "the",
    "their",
    "theirs",
    "them",
    "themselves",
    "then",
    "there",
    "these",
    "they",
    "this",
    "those",
    "through",
    "to",
    "too",
    "under",
    "until",
    "up",
    "ve",
    "very",
    "was",
    "we",
    "were",
    "what",
    "when",
    "where",
    "which",
    "while",
    "who",
    "whom",
    "why",
    "will",
    "with",
    "won",
    "y",
    "you",
    "youd",
    "youll",
    "youre",
    "youve",
    "your",
    "yours",
    "yourself",
    "yourselves",
    "stock",
    "bank",
    "inc",
    "trades",
    "corporation",
    "bbc",
    "inc",
    "incorperated",
    "amp",
    "nasdaq",
    "ve",
    "th",
    "uh",
    "hmmm",
    "rd" "pm",
]

STOPWORDS = set(stopwordlist)


def cleaning_stopwords(text):
    return " ".join([word for word in str(text).split() if word not in STOPWORDS])


df["text"] = df["text"].apply(lambda text: cleaning_stopwords(text))


def generate_N_grams(text, ngram=1):
    import nltk
    from nltk.corpus import stopwords

    nltk.download("stopwords")

    words = [
        word for word in text.split(" ") if word not in set(stopwords.words("english"))
    ]
    print("Sentence after removing stopwords:", words)
    temp = zip(*[words[i:] for i in range(0, ngram)])
    ans = [" ".join(ngram) for ngram in temp]
    return ans


from nltk.tokenize import RegexpTokenizer

tokenizer = RegexpTokenizer("\s+", gaps=True)
df["text"] = df["text"].apply(tokenizer.tokenize)

import nltk

st = nltk.PorterStemmer()


def stemming_on_text(data):
    text = [st.stem(word) for word in data]
    return data


df["text"] = df["text"].apply(lambda x: stemming_on_text(x))

lm = nltk.WordNetLemmatizer()


def lemmatizer_on_text(data):
    text = [lm.lemmatize(word) for word in data]
    return data


df["text"] = df["text"].apply(lambda x: lemmatizer_on_text(x))


X = df.text
y = df.label

# Separating the 95% data for training data and 5% for testing data
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.8, random_state=26105111
)

vectoriser = TfidfVectorizer(ngram_range=(1, 2), max_features=500000)
vectoriser.fit(X_train.apply(lambda x: " ".join(x)))
print("No. of feature_words: ", len(vectoriser.get_feature_names()))

X_train = vectoriser.transform(X_train.apply(lambda x: " ".join(x)))
X_test = vectoriser.transform(X_test.apply(lambda x: " ".join(x)))

from sklearn.metrics import confusion_matrix, classification_report


def model_Evaluate(model):
    # Predict values for Test dataset
    y_pred = model.predict(X_test)
    # Print the evaluation metrics for the dataset.
    print(classification_report(y_test, y_pred))


from sklearn.svm import LinearSVC
SVCmodel = LinearSVC()
SVCmodel.fit(X_train, y_train)
model_Evaluate(SVCmodel)
y_pred2 = SVCmodel.predict(X_test)

df = pd.DataFrame({'Actual': y_test, 'Predicted': y_pred2})
print(df.head(10))

from sklearn.linear_model import LogisticRegression

LRmodel = LogisticRegression(C=2, max_iter=1000, n_jobs=-1)
LRmodel.fit(X_train, y_train)
model_Evaluate(LRmodel)
y_pred3 = LRmodel.predict(X_test)

df = pd.DataFrame({"Actual": y_test, "Predicted": y_pred3})
print(df.head(20))
