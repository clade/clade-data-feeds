from pymongo import MongoClient


class GenerateText:
    def twitter_feed_collection(self):
        client = MongoClient("mongodb://localhost:27017/")
        collection = client["cassius"].get_collection("twitter_feed")
        return collection

    def get_tweet_data(self, twitter_feed):
        tweets = []
        data = twitter_feed.find(
            {},
            {"values.text": 1, "symbol": 1, "id": 1, "values.created_at": 1},
        )
        for tweet in data:
            tweet_text = tweet["values"]["text"]
            tweet_text = tweet_text.replace("\n", " ")
            tweet_text = tweet_text.replace("|", "")
            tweet_text = tweet_text.replace('“', "")
            tweet_text = tweet_text.replace('”', "")
            tweet_text = tweet_text.replace('"', "")
            try:
                created_at = tweet["values"]["created_at"]
            except KeyError:
                created_at = None
            tweets.append(
                {
                    "symbol": tweet["symbol"],
                    "created_at": created_at,
                    "text": tweet_text,
                    "id": tweet["id"],
                }
            )

        return tweets

    def export_to_dsv(self, tweets_data):
        file = "twitter_data.csv"
        import os

        if os.path.exists(file):
            os.remove(file)

        f = open(file, "w")
        f.write("id|text\r")
        f.close()

        for tweets in tweets_data:
            tweet_line = f"{tweets['id']}|{tweets['text']}\r"
            f = open(file, "a")
            f.write(tweet_line)
            f.close()

    def start(self):
        twitter_feed = self.twitter_feed_collection()
        tweet_data = self.get_tweet_data(twitter_feed)
        self.export_to_dsv(tweet_data)
