from datetime import datetime, timedelta
from pandas import pandas as pd
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
from pymongo import MongoClient
import re
from bson.json_util import dumps
import json
import emoji
from autocorrect import Speller
from textblob import TextBlob

analyser = SentimentIntensityAnalyzer()


class CleanText:

    file = "twitter_data.csv"
    clean_file = "twitter_data_cleaned.csv"
    spell = Speller()

    def __init__(self):
        self.df = pd.read_csv(self.file, sep="|")
        client = MongoClient("mongodb://localhost:27017/")
        collection = client["cassius"].get_collection("company_data")
        self.company_data = list(json.loads(dumps(collection.find())))
        print(f"Before: {self.count()}")

    def f_remove_rt(self):
        self.df["text"] = self.df["text"].replace(r"RT @\w+:", "", regex=True)
        self.df["text"] = self.df["text"].replace(r"@\w+ ", "", regex=True)
        self.df["text"] = self.df["text"].replace(r"@\w+.", "", regex=True)

        return self

    def f_remove_too_many_dollar_comanies(self):
        # No more than 2 $string and 3 $num
        mask = (self.df["text"].str.count(r"\$\D") <= 2) & (
            self.df["text"].str.count(r"\$\d") <= 3
        )
        self.df = self.df.loc[mask]

        return self

    def f_remove_all_dollar_company_values(self):
        self.df["text"] = self.df["text"].replace("$", " $", regex=True)
        self.df["text"] = self.df["text"].replace(r"\$\w+", "", regex=True)

        return self

    def f_remove_all_hash_tags(self):
        self.df["text"] = self.df["text"].replace("#", " #", regex=True)
        self.df["text"] = self.df["text"].replace(r"\#\w+", "", regex=True)

        return self

    def f_remove_all_percent(self):
        self.df["text"] = self.df["text"].replace("%", " %", regex=True)
        self.df["text"] = self.df["text"].replace(r"\%\w+", "", regex=True)

        return self

    def f_remove_all_plus(self):
        self.df["text"] = self.df["text"].replace(r"\+", " ", regex=True)
        self.df["text"] = self.df["text"].replace(r"(?<=\d)[,\.]", "", regex=True)

        return self

    def f_clean_numbers(self):
        self.df["text"] = self.df["text"].replace(r"\d+", "", regex=True)

        return self

    def f_remove_unicode(self):
        self.df["text"] = self.df["text"].replace(r"\&amp;", " ", regex=True)

        return self

    def f_remove_non_alphanumeric(self):
        self.df["text"] = self.df["text"].replace(r"[^ a-zA-Z]", " ", regex=True)

        return self

    def f_replace_links(self):
        """For analysis remove links."""
        self.df["text"] = self.df["text"].str.replace(
            r'https?://[^\s<>"]+|www\.[^\s<>"]+', " "
        )

        return self

    def single(self, text):
        try:
            return re.sub(r"(?:^| )\w(?:$| )", " ", text)
        except TypeError:
            return text

    def f_remove_sigle_chars(self):
        self.df["text"] = self.df["text"].apply(lambda text: self.single(text))

        return self

    def f_remove_double_spaces(self):
        self.df["text"] = self.df["text"].replace(r"\s+", " ", regex=True)

        return self

    def f_replace_whitespace(self):
        self.df["text"] = self.df["text"].replace(r"(^\s+|\s+$)", "", regex=True)

        return self

    def f_replace_known_shorts(self):
        self.df["text"] = self.df["text"].replace(r" pt ", " price target ", regex=True)
        self.df["text"] = self.df["text"].replace(r" wk ", " week ", regex=True)
        self.df["text"] = self.df["text"].replace(r" lt ", " less than ", regex=True)
        self.df["text"] = self.df["text"].replace(r" gt ", " greater than ", regex=True)
        self.df["text"] = self.df["text"].replace(
            r" pac ", " political action commitee ", regex=True
        )
        self.df["text"] = self.df["text"].replace(
            r" fed ", " federal exchange ", regex=True
        )
        self.df["text"] = self.df["text"].replace(r" div ", " dividend ", regex=True)
        self.df["text"] = self.df["text"].replace(
            r" ytd ", " year to date ", regex=True
        )
        self.df["text"] = self.df["text"].replace(
            r" yoy ", " year on year ", regex=True
        )
        self.df["text"] = self.df["text"].replace(r" co ", " company ", regex=True)
        self.df["text"] = self.df["text"].replace(
            r" inc ", " incorperated ", regex=True
        )
        self.df["text"] = self.df["text"].replace(
            r" eps ", " earnings per share ", regex=True
        )
        self.df["text"] = self.df["text"].replace(
            r" copr ", " coprporation ", regex=True
        )
        self.df["text"] = self.df["text"].replace(r" shs ", " shares ", regex=True)
        self.df["text"] = self.df["text"].replace(r" hodl ", " hold ", regex=True)
        self.df["text"] = self.df["text"].replace(
            r" ev ", " electric vehicle ", regex=True
        )
        self.df["text"] = self.df["text"].replace(r" mkts ", " market ", regex=True)
        self.df["text"] = self.df["text"].replace(r" mkt ", " market ", regex=True)
        self.df["text"] = self.df["text"].replace(
            r" corp ", " corporation ", regex=True
        )
        self.df["text"] = self.df["text"].replace(
            r" ec ", " european commission ", regex=True
        )
        self.df["text"] = self.df["text"].replace(
            r" eu ", " european european ", regex=True
        )
        self.df["text"] = self.df["text"].replace(r" fy ", " full year ", regex=True)
        self.df["text"] = self.df["text"].replace(
            r" ath ", " all time high ", regex=True
        )
        self.df["text"] = self.df["text"].replace(
            r" dma ", " displaced moving average ", regex=True
        )

        return self

    def raci(self, text):
        for company in self.company_data:
            sym = f"${company['symbol']}"
            if sym in text:
                text = text.replace(sym, " ")

        return text

    def raci2(self, text):
        for company in self.company_data:
            for company_short_name in company["short_name"]:
                csn = company_short_name.lower()
                text = re.sub(r"\b{0}\b".format(csn), "", text)

        return text

    def f_remove_all_company_info(self):
        self.df["text"] = self.df["text"].apply(lambda text: self.raci(text))

        return self

    def f_remove_all_company_info2(self):
        self.df["text"] = self.df["text"].apply(lambda text: self.raci2(text))

        return self

    def f_misc_noise(self):
        self.df["text"] = self.df["text"].replace(r"(https|http)", "", regex=True)

        return self

    def f_drop_duplicates(self):
        """Always."""
        self.df = self.df.drop_duplicates()

        return self

    def f_drop_nr_duplicates(self):
        """Bots and automated seem to like this noise."""
        self.df = self.df.drop_duplicates()
        self.df["nr_duplicates"] = self.df.text.str[:28]
        self.df.drop_duplicates(subset="nr_duplicates", inplace=True)
        self.df = self.df.drop(columns=["nr_duplicates"])
        self.df = self.df.drop_duplicates()

        self.df["nr_rev_duplicates"] = self.df.loc[:, "text"].apply(lambda x: x[::-1])
        self.df["nr_rev_duplicates"] = self.df.nr_rev_duplicates.str[:30]
        self.df.drop_duplicates(subset="nr_rev_duplicates", inplace=True)
        self.df = self.df.drop(columns=["nr_rev_duplicates"])
        self.df = self.df.drop_duplicates()

        return self

    def f_drop_questions(self):
        """Questions are not statements and therfore become too neutral."""
        self.df["text"] = self.df["text"].replace(r"(why|Why)", "?", regex=True)
        self.df = self.df[~self.df.text.str.contains("\?")]

        return self

    def f_drop_versus(self):
        """Questions are not statements and therfore become too neutral."""
        self.df = self.df[~self.df.text.str.contains("vs")]

        return self

    def f_drop_nulls(self):
        """Always."""
        self.df = self.df.dropna(subset=["text"])

        return self

    def f_replace_emojis(self):
        self.df["text"] = self.df["text"].apply(
            lambda text: emoji.demojize(text).replace("_", " ")
        )

        return self

    def f_lower(self):
        self.df["text"] = self.df["text"].str.lower()

        return self

    def correct_it(self, text):
        try:
            return self.spell(text)
        except TypeError:
            return text

    def f_text_correct(self):
        self.df["text"] = self.df["text"].apply(lambda text: self.correct_it(text))

        return self

    def f_remove_mins_and_maxi(self):
        """Tweet value limited as 20 chars."""
        mask = (self.df["text"].str.len() >= 25) & (self.df["text"].str.len() <= 100)
        self.df = self.df.loc[mask]

        return self

    def count(self):
        return len(self.df)

    def sentiment_analyzer_scores(self, sentence):
        score = analyser.polarity_scores(sentence)
        return score["compound"]

    def create_clean_file(self):
        # self.df.sort_values(
        #     by=["symbol", "created_at", "compound"], ascending=[True, False, False]
        # ).to_csv(self.clean_file, sep="|", index=False)
        self.df.to_csv(self.clean_file, sep="|", index=False)

    def start(self):
        df_clean = (
            self.f_remove_rt()
            .f_remove_too_many_dollar_comanies()
            .f_remove_all_dollar_company_values()
            .f_remove_all_percent()
            .f_remove_all_plus()
            .f_remove_all_hash_tags()
            .f_clean_numbers()
            .f_remove_unicode()
            .f_replace_links()
            .f_drop_questions()
            .f_remove_non_alphanumeric()
            .f_remove_sigle_chars()
            .f_remove_double_spaces()
            .f_lower()
            .f_remove_all_company_info()
            .f_remove_all_company_info2()
            .f_remove_sigle_chars()
            .f_misc_noise()
            .f_replace_known_shorts()
            .f_replace_whitespace()
            .f_remove_double_spaces()
            .f_drop_versus()
            .f_drop_nr_duplicates()
            .f_drop_nulls()
            .f_remove_mins_and_maxi()
        )

        print(f"After: {df_clean.count()}")
        df_clean.create_clean_file()
