#!/usr/bin/env python3
from datetime import datetime
from newsapi.newsapi_client import NewsApiClient
from newsapi.newsapi_exception import NewsAPIException
from time import sleep
from bson.json_util import dumps
from config.news_config import api_key
from lib.mongo import Mongo
from lib.schedule_helper import ScheduleHelper
import time
import json


class News:
    def __init__(self):
        self.db = Mongo().db
        self.company_data = list(json.loads(dumps(self.db.company_data.find())))
        self.news_schedule_config = self.db.schedule.find_one({"job": "get_news"})
        self.news_headlines_schedule_config = self.db.schedule.find_one(
            {"job": "get_news_headlines"}
        )
        self.news_api = NewsApiClient(api_key=api_key)
        self.start_time = time.time()

    def save_news_data_to_db(self, symbol, news_data):
        print(f"News for: {symbol} Article Count: {len(news_data)}")
        processed = 0
        for news_article in news_data:
            if (
                self.db.news.count_documents(
                    {"symbol": symbol, "title": news_article["title"]}
                )
                == 0
            ):
                processed = processed + 1
                self.db.news.insert_one(
                    {
                        "symbol": symbol,
                        "title": news_article["title"],
                        "article": {
                            "source": news_article["source"],
                            "description": news_article["description"],
                            "url": news_article["url"],
                            "url_image": news_article["urlToImage"],
                            "content": news_article["content"],
                            "author": news_article["author"],
                        },
                        "published_at": datetime.strptime(
                            news_article["publishedAt"], "%Y-%m-%dT%H:%M:%SZ"
                        ),
                    }
                )
        print(f"{symbol} new articles: {processed}")

    def __one_hour_wait(self):
        print("Too many requests wait for 1 hour...")
        return sleep(60 * 60)

    def get_articles_for(self, q):
        try:
            return self.news_api.get_everything(q=q, language="en", sort_by="relevancy")
        except NewsAPIException:
            self.__one_hour_wait()
            return []

    def start(self):

        while not sleep(1):
            news_schedule = ScheduleHelper(self.news_schedule_config)
            if news_schedule.within_time():
                news_period = news_schedule.period()
                for company in self.company_data:
                    news_data = self.get_articles_for(company["name"])
                    self.save_news_data_to_db(company["symbol"], news_data["articles"])
                    print(f"Next news call in {news_period / 60} minutes")
                    sleep(news_period)


News().start()
