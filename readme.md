# Cassius Data Feeds

The Cassius data feeds currently provide the following main data sources.
   
- Finance data
- News Data 

## Finance Data

The finance data is broken down into further areas.

- Historic ticker data from 24 hours to 5 years
- Dividend data
- Company details
- Company fundamentals, such as 52 Day high/lows

## Jupyter Notebooks

We use Jupyter Notebooks to layout the requirements, which also includes code snippets, JSON objects, Database names and any public API methods. There is some cross-over with Cassius Components API project, used for accessing the stored data.

click [here](https://gitlab.com/clade/clade-data-feeds/-/tree/master/jupyter-notebooks) to see notebooks.

## Static Reference Data

We have some static reference data, which is required for the moment, this may change as the site matures and data becomes externalised. For the moment we're limiting stocks to the ~Top 100 Market Cap, explained further in the [Company Details Notebook](https://gitlab.com/clade/clade-data-feeds/-/blob/master/jupyter-notebooks/company-details.ipynb)

To load the data in;

1. Navigate to mongo_config directory
2. execute: `python load_it.py --data company_data.json`
3. Repeat this for all json files

